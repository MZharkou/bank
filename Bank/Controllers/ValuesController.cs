﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bank.Models;
using System.Data;

namespace Bank.Controllers
{
    public class ValuesController : ApiController
    {
        BankContext db = new BankContext();

        public IEnumerable<Zayavka> GetBooks()
        {
            return db.Zayavkas;
        }

        public Zayavka GetBook(int id)
        {
            Zayavka zayavka = db.Zayavkas.Find(id);
            return zayavka;
        }

        [HttpPost]
        public void CreateZayavka([FromBody]Zayavka zayavka)
        {
            db.Zayavkas.Add(zayavka);
            db.SaveChanges();
        }

        [HttpPut]
        public void EditZayavka(int id, [FromBody]Zayavka zayavka)
        {
            if (id == zayavka.Id)
            {
                db.Entry(zayavka).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public void DeleteZayavka(int id)
        {
            Zayavka zayavka = db.Zayavkas.Find(id);
            if (zayavka != null)
            {
                db.Zayavkas.Remove(zayavka);
                db.SaveChanges();
            }
        }


        public IEnumerable<ObjectInkas> GetObjectInkass()
        {
            return db.ObjectInkass;
        }

        public ObjectInkas GetObjectInkas(int id)
        {
            ObjectInkas objectInkas = db.ObjectInkass.Find(id);
            return objectInkas;
        }

        [HttpPost]
        public void CreateObjectInkas([FromBody]ObjectInkas objectInkas)
        {
            db.ObjectInkass.Add(objectInkas);
            db.SaveChanges();
        }

        [HttpPut]
        public void EditObjectInkas(int id, [FromBody]ObjectInkas objectInkas)
        {
            if (id == objectInkas.ObjId)
            {
                db.Entry(objectInkas).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public void DeleteObjectInkas(int id)
        {
            ObjectInkas objectInkas = db.ObjectInkass.Find(id);
            if (objectInkas != null)
            {
                db.ObjectInkass.Remove(objectInkas);
                db.SaveChanges();
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}