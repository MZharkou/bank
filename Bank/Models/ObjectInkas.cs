﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class ObjectInkas
    {
        public int ObjId { get; set; }
        public string Citytype { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Haus { get; set; }
        public string Korpus { get; set; }
        public string Usluga { get; set; }
        public int BankId { get; set; }
    }
}