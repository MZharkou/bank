﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class Zayavka
    {

        public int Id { get; set; }
        public string Tipzayavki { get; set; }
        public string Numzayavki { get; set; }
        public DateTime Date { get; set; }
        public string Statused { get; set; }
        public string Bankkorpus { get; set; }
        public string Inn { get; set; }
        public string Kpp { get; set; }
        public string Namefirm { get; set; }
        public string Ogrn { get; set; }
        public string Namesotrudnik { get; set; }
        public string Telephonenumsotrudnik { get; set; }
        public string Numschet { get; set; }
        public string Bik { get; set; }
        public string Numkorr { get; set; }
        public string Bankname { get; set; }
        public string Swift { get; set; }
        public string Rekvizity { get; set; }
        //объект инкассации
        public string Time { get; set; }
        public string Sposob { get; set; }
        public string Chastota { get; set; }
        public string Dennedeli { get; set; }
        public decimal Objem { get; set; }
        public string Valuta { get; set; }
        public string Director { get; set; }
        public DateTime Nachalo { get; set; }
        public string Rabdnibegin { get; set; }
        public string Rabdnifinish { get; set; }
        public string Subbotabegin { get; set; }
        public string Subbotafinish { get; set; }
        public string Voskrbegin { get; set; }
        public string Voskrfinish { get; set; }
        public string Addresstype { get; set; }
        public string Citytype { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Haus { get; set; }
        public string Korpus { get; set; }
        public string Usluga { get; set; }

    }
}