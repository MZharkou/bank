﻿using System.Data.Entity;

namespace Bank.Models
{
    public class BankContext : DbContext
    {
        public DbSet<Zayavka> Zayavkas { get; set; }
        public DbSet<ObjectInkas> ObjectInkass { get; set; }
    }
}